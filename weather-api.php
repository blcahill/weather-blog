<?php

require_once 'vendor/autoload.php';

$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();
$api_key = getenv('API_KEY');
$client = new GuzzleHttp\Client();
$current = $client->request('GET', 'api.openweathermap.org/data/2.5/weather?q=' .$city. ',' .$country. '&mode=html&APPID='. $api_key);

$forecasts = $client->request('GET', 'api.openweathermap.org/data/2.5/forecast?q=' .$city. ',' .$country. '&mode=html&APPID='. $api_key);

$current->getStatusCode();

// echo $current->getReasonPhrase(); // OK
// echo $current->getProtocolVersion(); // 1.1

function checkError($status_code){
if($status_code == 404){
	return "Somethings Wrong with the city you are trying to look at!";
	}
else{
	"Success!";
	}
}
 
echo $current->getBody();

checkError($current->getStatusCode());
checkError($forecasts->getStatusCode());

$array = json_decode($forecasts->getBody(), true);

class organizeData {
	public $days = [];
	public $temps = [];
	public $weathers = [];
	public $descriptions =[];


	public function myDecode($json){
		$array = json_decode($json->getBody(), true);
		$this->days = $array["list"];

	}

	public function getTemp($array){
		foreach ($array as $item) {
			$temp = $item["main"]["temp"];
			$this->temps[] =  $temp;
		}
	}

	public function getWeather($array){
		foreach ($array as $item) {
			$weather = $item["weather"][0]["main"];
			array_push($this->weathers, $weather);
			$this->weathers[] =  $weather;
		}
	}


	public function getDescription($array){
		foreach ($array as $item) {
			$description = $item["weather"][0]["description"];
			array_push($this->descriptions, $description);
			$this->descriptions[] =  $description;
		}
	}

}


$data = new organizeData();
$data->myDecode($forecasts);
// var_dump($data->days[0]);
$data->getTemp($data->days);
$data->getWeather($data->days);
$data->getDescription($data->days);
// var_dump($data->temps);
// var_dump($data->weathers);
// var_dump($data->descriptions);

// $shorten_temps = array_slice($temps, 0, 4);
// $shorten_weathers = array_slice($weathers, 0, 4);
// $shorten_description = array_slice($shorten_description, 0, 4);
?>




