<?php
/**
* Template Name: City
*/


?>
<?php $city = get_the_title();?>

<?php while (have_posts()) : the_post(); ?>
	<?php $country = get_the_content()?>
<?php endwhile; ?>


<h1><?php the_title(); ?></h1>
<?php while (have_posts()) : the_post(); ?>
 	<h4><?php the_content()?></h4>
<?php endwhile; ?>


<?php 
	include 'weather-api.php';
?>

<div class="days">
<div class="day">
<h2><?php  echo $array["list"][0]['main']['temp']; ?></h2>

<h4><?php  echo $array["list"][0]["weather"][0]['main']; ?></h4>
<h4><?php  echo $array["list"][0]["weather"][0]['description']; ?></h4>

</div>

<div class="day">
<h2><?php  echo $array["list"][1]['main']['temp']; ?></h2>

<h4><?php  echo $array["list"][1]["weather"][0]['main']; ?></h4>
<h4><?php  echo $array["list"][1]["weather"][0]['description']; ?></h4>

</div>


<div class="day">
<h2><?php  echo $array["list"][2]['main']['temp']; ?></h2>

<h4><?php  echo $array["list"][2]["weather"][0]['main']; ?></h4>
<h4><?php  echo $array["list"][2]["weather"][0]['description']; ?></h4>
</div>

<div class="day">
<h2><?php  echo $array["list"][3]['main']['temp']; ?></h2>

<h4><?php  echo $array["list"][3]["weather"][0]['main']; ?></h4>
<h4><?php  echo $array["list"][3]["weather"][0]['description']; ?></h4>
</div>


</div>



